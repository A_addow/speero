import 'package:flutter/material.dart';
import 'package:shop_monkey/ui/create_user.dart';
import 'package:shop_monkey/ui/login.dart';
import 'package:shop_monkey/ui/settings.dart';
import 'package:shop_monkey/ui/users_list.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LoginPage(),//MyHomePage(),
      routes: {
        '/Settings': (ctx) => Settings(),
        '/Users': (ctx) => UsersList(),
        '/CreateUser': (context) => CreateUser(),
      },
    );
  }
}
