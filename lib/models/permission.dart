class Permission {
  bool dashboard;
  bool viewAllOrders;
  bool editAuthorizedOrders;
  bool editOrdersUponConvertingToInvoice;
  bool viewPofitabilityInfo;
  bool viewAllAppointments;
  bool includeUserOnCalendar;
  bool inventory, viewAllTimeClocks;
  bool editTimeSheetEntries;
  bool addTimeManually;
  bool viewAndEditInspectionTemplates;
  bool viewAndEditCannedServices;
  bool reports;
  bool viewAndEditUsers;
  bool viewAndEditPricingMatrices;
  Permission({
    this.dashboard,this.addTimeManually,this.editAuthorizedOrders,this.editOrdersUponConvertingToInvoice,
    this.editTimeSheetEntries,this.includeUserOnCalendar,this.inventory,this.reports,
    this.viewAllAppointments,this.viewAllOrders,this.viewAllTimeClocks,
    this.viewAndEditCannedServices,this.viewAndEditInspectionTemplates,
    this.viewAndEditPricingMatrices,this.viewAndEditUsers,this.viewPofitabilityInfo
    
  });
}
