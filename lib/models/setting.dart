import 'package:shop_monkey/models/permission.dart';

class Setting {
  bool customerCommmunication;
  bool timeTracker;
  bool flatRate;
  Permission permission;
  Setting({
    this.customerCommmunication,
    this.timeTracker,
    this.flatRate,
    this.permission,
  });
}
