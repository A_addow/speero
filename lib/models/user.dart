import 'package:flutter/foundation.dart';
import 'package:shop_monkey/models/setting.dart';

class User {
  /*
0:"id" -> "0947ba45-3d71-347d-99bb-7b899be20298"
1:"first_name" -> "Christiana"
2:"last_name" -> "Bednar"
3:"is_active" -> true
4:"email" -> "lavonne.wolff@example.net"
5:"rate" -> 78.37
6:"role" -> "services_writer"
7:"permissions" -> Map (13 items)
8:"phone" -> "0570039330"
  */
  String id;
  String firstName;
  String lastName;
  String email;
 bool isActive;
  double rate;
  String phone;
  String role;
  Setting setting;
  User({
     this.id,
   @required this.firstName,
   @required this.lastName,
   @required this.email,
    this.isActive,
     this.rate,
    this.phone,
     this.role,
     this.setting,
  });

 
}
class UpdatedUser {
 static String firstName;
  static String lastName;
  static String email;
  static String phone;

  }