import 'package:shared_preferences/shared_preferences.dart';

import 'constants.dart';

class MySharedPrefrences {
  static Future<SharedPreferences> getSharedPrefrencesInstance()  async {
    return await  SharedPreferences.getInstance();
  }
static Future<String> getByKey(String key)  async {
  var sharedPreferencesInstance= await  getSharedPrefrencesInstance();
  return  sharedPreferencesInstance.getString(key);
}
static setByKey(String key) async {
var sharedPreferencesInstance=await getSharedPrefrencesInstance() ;
sharedPreferencesInstance.setString(Constants.MYTOKEN, key);
}
}