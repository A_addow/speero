import 'package:flutter/material.dart';
import 'package:shop_monkey/models/permission.dart';
import 'package:shop_monkey/models/user.dart';
import 'package:shop_monkey/ui/notifications.dart';
import 'package:shop_monkey/ui/permissions.dart';
import 'package:shop_monkey/widgets/dropdown_button.dart';
import 'package:shop_monkey/widgets/user_type_settings.dart';

class CreateUser extends StatefulWidget {
  @override
  _CreateUserState createState() => _CreateUserState();
}

class _CreateUserState extends State<CreateUser> {
  final _formKey = GlobalKey<FormState>();
  User user = User(firstName: '', lastName: '', email: '');
  _showText(String text) {
    return Container(
      alignment: Alignment.centerLeft,
      margin: EdgeInsets.only(left: 8.0),
      child: Text(text),
    );
  }

  final List<String> _roles = <String>['Admin', 'Service provider'];

  final List<String> _listStrings = <String>['Permissions', 'Notifications'];

  var selectedOption;

  _getMyChoice(String selectedValue) {
    setState(() {
      selectedOption = selectedValue;
    });
  }
Permission permission= Permission(
    dashboard: false,
    viewAllOrders: true,
    editAuthorizedOrders: true,
    editOrdersUponConvertingToInvoice: true,
    viewPofitabilityInfo: false,
    viewAllAppointments: true,
    includeUserOnCalendar: true,
    inventory: true,
    viewAllTimeClocks: false,
    editTimeSheetEntries: false,
    addTimeManually: true,
    viewAndEditInspectionTemplates: false,
    viewAndEditCannedServices: false,
    reports: false,
    viewAndEditUsers: false,
    viewAndEditPricingMatrices: false,
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(left: 10.0),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Create User'),
                      IconButton(
                          icon: Icon(Icons.close),
                          onPressed: () => Navigator.of(context).pop()),
                    ],
                  ),
                  _showText('First Name'),
                  Container(
                    // height: MediaQuery.of(context).size.height / 9,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        decoration: InputDecoration(
                          hintText: 'e.g Ali',
                          contentPadding:
                              EdgeInsets.only(left: 10.0, top: 10.0),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(6.0),
                          ),
                        ),
                        validator: (value) {
                          if (value.isNotEmpty) {
                            _formKey.currentState.save();
                            return null;
                          } else
                            return 'First Name is required';
                        },
                        onSaved: (value) {
                          user.firstName = value;
                        },
                      ),
                    ),
                  ),
                  _showText('Last Name'),
                  Container(
                    // height: MediaQuery.of(context).size.height / 9,
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      decoration: InputDecoration(
                        hintText: 'e.g Omar',
                        contentPadding: EdgeInsets.only(left: 10.0, top: 10.0),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(6.0),
                        ),
                      ),
                      validator: (val) {
                        if (val.isNotEmpty) {
                          _formKey.currentState.save();
                          return null;
                        } else
                          return 'Last name is required';
                      },
                      onSaved: (value) {
                        user.lastName = value;
                      },
                    ),
                  ),
                  _showText('Email'),
                  Container(
                    // height: MediaQuery.of(context).size.height / 9,
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                        hintText: 'e.g Ali@example.com',
                        contentPadding: EdgeInsets.only(left: 10.0, top: 10.0),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(6.0),
                        ),
                      ),
                      validator: (val) {
                        if (val.isNotEmpty) {
                          _formKey.currentState.save();
                          return null;
                        } else
                          return 'Email is required';
                      },
                      onSaved: (value) {
                        user.email = value;
                      },
                    ),
                  ),
                  _showText('Phone(optional)'),
                  Container(
                    // height: MediaQuery.of(context).size.height / 9,
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      decoration: InputDecoration(
                        hintText: 'e.g (967) 775263811',
                        contentPadding: EdgeInsets.only(left: 10.0, top: 10.0),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(6.0),
                        ),
                      ),
                      onSaved: (value) {
                        user.phone = value;
                      },
                    ),
                  ),
                  _showText('Rate(optional)'),
                  Container(
                    // height: MediaQuery.of(context).size.height / 9,
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      decoration: InputDecoration(
                        hintText: 'e.g \$25.00',
                        contentPadding: EdgeInsets.only(left: 10.0, top: 10.0),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(6.0),
                        ),
                      ),
                      onSaved: (value) {
                        user.rate = double.parse(value);
                      },
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(
                        left: 8.0, top: 4.0, right: 8.0, bottom: 4.0),
                    child: Divider(
                      thickness: 1.5,
                    ),
                  ),
                  _showText('Permissions & Notifications'),
                  MyDropDownButton(_roles, 'User Type', _getMyChoice),
                  if (selectedOption != null) ...[
                    UserTypeSettings('Customer Communnication',
                        'Allows email and SMS messaging to customers', true),
                    UserTypeSettings('Time Tracker',
                        'Adds a time clock to track this person\'s work', true),
                    UserTypeSettings(
                        'Flat Rate',
                        'Auto-populate rate when assigned to a labor line item',
                        false),
                    MyDropDownButton(_listStrings, '', _getMyChoice),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Divider(
                        color: Colors.blue,
                        thickness: 2.0,
                      ),
                    ),
                    Text(
                      'Expand a section to set granular permissions',
                      style: TextStyle(color: Colors.grey[500]),
                    ),
                    Container(
                      margin: EdgeInsets.all(8.0),
                      height: 500,
                      width: 350,
                      decoration: BoxDecoration(
                          border:
                              Border.all(width: 1.5, color: Colors.grey[300])),
                      child: selectedOption == 'Notifications'
                          ? Notifications()
                          : Permissions(permission),
                    ),
                  ],
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      RaisedButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0)),
                        onPressed: () => Navigator.of(context).pop(),
                        child: Text('Cancel'),
                        padding: EdgeInsets.all(3.0),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0)),
                          onPressed: () {
                            _formKey.currentState.validate();
                            // _formKey.currentState.save();
                            print(permission.reports);
                          },
                          child: Text('Save'),
                          padding: EdgeInsets.all(3.0),
                          color: Theme.of(context).primaryColor,
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
