import 'package:flutter/material.dart';
import 'package:shop_monkey/widgets/notification_dialog.dart';
import '../widgets/quick_categories.dart';
import '../widgets/side_drawer.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool isSearching = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      drawer: SideDrawer(),
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(74, 89, 106, 0.75),
        actions: [
          Container(
            width: 300.0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                  icon: Icon(Icons.search),
                  onPressed: () {
                    setState(() {
                      isSearching = !isSearching;
                    });
                  },
                  alignment: Alignment.centerLeft,
                ),
                if (!isSearching) ...[
                  IconButton(
                    icon: Image.asset(
                      'assets/logo.png',
                      fit: BoxFit.cover,
                    ),
                    onPressed: () {},
                  ),
                  (IconButton(
                    icon: Icon(Icons.add),
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) => QuickCategories(),
                      );
                    },
                  )),
                  IconButton(
                    icon: Icon(Icons.notifications),
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) => NotificationDialog(),
                      );
                    },
                  ),
                ],
                if (isSearching)
                  Expanded(
                      child: TextField(
                    decoration: InputDecoration(
                      hintText: 'search here',
                    ),
                  )),
              ],
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(left: 20.0, top: 20.0, right: 50.0),
          child: Column(
            children: [
              Text(
                'Everything to run your shop',
                style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),
              ),
              Container(
                margin: EdgeInsets.only(top: 20.0),
                child: Text(
                  'All the tools you need in one place. Watch the videos to understand each module and see the beauty of Shopmonkey.io.',
                  style: TextStyle(
                    color: Colors.grey,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                color: Colors.white70,
                width: 300.0,
                height: 178.0,
                margin: EdgeInsets.all(20.0),
                child: Container(
                  margin: EdgeInsets.all(20.0),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Messaging',
                            style: TextStyle(
                              fontSize: 18.0,
                            ),
                          ),
                          IconButton(
                            icon: Icon(Icons.phonelink_ring),
                            onPressed: () {},
                          ),
                        ],
                      ),
                      Text(
                        'Text and email estimates and invoices, inspections,notes and appointment remainder.',
                        style: TextStyle(fontSize: 12.0),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            'watch video',
                            style:
                                TextStyle(color: Colors.blue, fontSize: 18.0),
                          ),
                          IconButton(
                            icon: Icon(Icons.arrow_forward),
                            onPressed: () {},
                            color: Colors.blue,
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ),
              Container(
                color: Colors.white70,
                width: 300.0,
                height: 178.0,
                margin: EdgeInsets.all(20.0),
                child: Container(
                  margin: EdgeInsets.all(20.0),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Inspections',
                            style: TextStyle(
                              fontSize: 18.0,
                            ),
                          ),
                          IconButton(
                            icon: Icon(
                              Icons.receipt,
                            ),
                            onPressed: () {},
                          ),
                        ],
                      ),
                      Text(
                        'Create digital inspection sheets to document your work and provide recommendations.',
                        style: TextStyle(fontSize: 12.0),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            'watch video',
                            style:
                                TextStyle(color: Colors.blue, fontSize: 18.0),
                          ),
                          IconButton(
                            icon: Icon(Icons.arrow_forward),
                            onPressed: () {},
                            color: Colors.blue,
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ), //LoginPage(),
    );
  }
}
