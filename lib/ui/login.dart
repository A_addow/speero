import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shop_monkey/ui/home.dart';
import '../shared/constants.dart';
import '../shared/my_shared_prefrences.dart';
import '../ui/register.dart';
import 'package:http/http.dart'
    as http; //as http means create object to use later
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginPageState();
  }
}

class LoginPageState extends State<LoginPage> {
  String email = '';
  String password = '';
  SharedPreferences pref;
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login Page'),
      ),
      body: ListView(
        children: <Widget>[
          Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Image.asset('assets/shopmonkeylogo.PNG'),
                SizedBox(
                  height: 32,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
                  child: Text(
                    'Log In To Your Account',
                    style: TextStyle(fontSize: 25.0),
                  ),
                ),
                Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                  child: TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                      labelText: 'Email',
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 0.5),
                      ),
                      border: OutlineInputBorder(),
                    ),
                    validator: (val) {
                      if (val.isNotEmpty) {
                        _formKey.currentState.save();
                        return null;
                      } else
                        return 'Email is required';
                    },
                    onSaved: (value) {
                      email = value;
                    },
                  ),
                ),
                Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      labelText: 'Password',
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 0.5),
                      ),
                      border: OutlineInputBorder(),
                    ),
                    validator: (val) {
                      if (val.isNotEmpty) {
                        _formKey.currentState.save();
                        return null;
                      } else
                        return 'Password is required';
                    },
                    onSaved: (value) {
                      password = value;
                    },
                  ),
                ),
                ButtonTheme(
                  minWidth: 320,
                  height: 40.0,
                  child: RaisedButton(
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        _openHomePage();
                      }
                    },
                    child: Text(
                      'Log In',
                      style: TextStyle(fontSize: 20, color: Colors.white),
                    ),
                    color: Color.fromRGBO(15, 168, 115, 1.0),
                  ),
                ),
                Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                  child: Divider(
                    thickness: 1.5,
                  ),
                ),
                ButtonTheme(
                  minWidth: 320,
                  height: 40.0,
                  child: RaisedButton(
                    color: Color.fromRGBO(0, 108, 250, 1.0),
                    onPressed: () {},
                    child: Text(
                      'Log In With Google',
                      style: TextStyle(fontSize: 20, color: Colors.white),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('don`t have an account '),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: 16.0, vertical: 16.0),
                      child: FloatingActionButton(
                        child: Text(
                          'Sign Up',
                          style: TextStyle(fontSize: 8.0),
                        ),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (BuildContext context) => RegisterPage(),
                            ),
                          );
                        },
                        mini: true,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _openHomePage() {
    var map = new Map<String, dynamic>();
    map['email'] = '$email';
    map['password'] = '$password';
    var url = '${Constants.BASICURL}login';
    http.post(url, body: map).then((http.Response response) async {
      var data = json.decode(response.body)['data'];
      MySharedPrefrences.setByKey('${data['token']}');
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (BuildContext context) => MyHomePage(),
        ),
      );
    });
  }
}
