import 'package:flutter/material.dart';

class Notifications extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
        children: [
    Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Center(child: Text('Notification Type')),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Padding(
             padding: const EdgeInsets.only(left: 9.0),
              child: Column(
                children: [
                  Text('Email'),
                  FlatButton(
                    onPressed: () {},
                    child: Text(
                      'Deselect All',
                      style: TextStyle(color: Theme.of(context).primaryColor),
                    ),
                  )
                ],
              ),
            ),
            Column(
              children: [
                Text('In-App'),
                FlatButton(
                  onPressed: () {},
                  child: Text(
                    'Deselect All',
                    style: TextStyle(color: Theme.of(context).primaryColor),
                  ),
                )
              ],
            ),
          ],
        ),
      ],
    ),
    Divider(
      thickness: 1.0,
    ),
    Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
            width: 150.0, child: Text('Message received from customer')),
        Padding(
          padding: const EdgeInsets.only(right: 40.0),
          child: Checkbox(value: true, onChanged: (val) {}),
        ),
        Checkbox(value: true, onChanged: (val) {}),
      ],
    ),
    Divider(
      thickness: 1.0,
    ),
    Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
            width: 150.0,
            child: Text('Message sent to customer by teammate')),
        Padding(
          padding: const EdgeInsets.only(right: 40.0),
          child: Checkbox(value: true, onChanged: (val) {}),
        ),
        Checkbox(value: true, onChanged: (val) {})
      ],
    ),
    Divider(
      thickness: 1.0,
    ),
    Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(width: 150.0,child: Text('Service assigned or unassigned')),
        Padding(
          padding: const EdgeInsets.only(right: 40.0),
          child: Checkbox(value: true, onChanged: (val) {}),
        ),
        Checkbox(value: true, onChanged: (val) {})
      ],
    ),
    Divider(
      thickness: 1.0,
    ),
    Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(width: 150,child: Text('Order authorized by customer')),
        Padding(
        padding: const EdgeInsets.only(right: 40.0),
          child: Checkbox(value: true, onChanged: (val) {}),
        ),
        Checkbox(value: true, onChanged: (val) {})
      ],
    ),
    Divider(
      thickness: 1.0,
    ),
    Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(width: 150.0,child: Text('Appointment confirmed or cancelled')),
        Padding(
       padding: const EdgeInsets.only(right: 40.0),
          child: Checkbox(value: true, onChanged: (val) {}),
        ),
        Checkbox(value: true, onChanged: (val) {})
      ],
    ),
    Divider(
      thickness: 1.0,
    ),
    Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(width: 150.0,child: Text('Payment made by customer')),
        Padding(
          padding: const EdgeInsets.only(right: 40.0),
          child: Checkbox(value: true, onChanged: (val) {}),
        ),
        Checkbox(value: true, onChanged: (val) {})
      ],
    ),
        
        ],
      );
  }
}
