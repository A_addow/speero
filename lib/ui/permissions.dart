
import 'package:flutter/material.dart';
import 'package:shop_monkey/models/permission.dart';

class Permissions extends StatefulWidget {
  final Permission permission;
  Permissions(this.permission);
  @override
  _PermissionsState createState() => _PermissionsState(permission);
}

class _PermissionsState extends State<Permissions> {
  _PermissionsState(this.permission);
  final Permission permission;
  bool dashbord = true, workflowOrders = true, calendar = true;
  bool inventory = true, timeClocks = false;
  bool lists = true, reports = false, settings = false;
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          ExpansionTile(
            title: Padding(
              padding: const EdgeInsets.only(left: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      child: Row(
                    children: [
                      Icon(
                        Icons.av_timer,
                      ),
                      Text('Dashboard'),
                    ],
                  )),
                  Switch(
                    value: permission.dashboard,
                    onChanged: (v) {
                      setState(() {
                        permission.dashboard = v;
                      });
                    },
                  ),
                ],
              ),
            ),
            trailing: SizedBox(),
            children: [],
          ),
          Divider(
            thickness: 1.0,
          ),
          ExpansionTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                    child: Row(
                  children: [
                    Icon(Icons.keyboard_arrow_down),
                    Icon(
                      Icons.dashboard,
                    ),
                    Text('Workflow & Orders'),
                  ],
                )),
                Switch(
                  value: workflowOrders,
                  onChanged: (v) {
                    setState(() {
                      workflowOrders = v;
                    });
                  },
                ),
              ],
            ),
            trailing: SizedBox(),
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 30.0),
                    child: Text('Workflow'),
                  ),
                  ListTile(
                    leading: Checkbox(
                      value:!workflowOrders?false: permission.viewAllOrders,
                      onChanged: workflowOrders
                          ? (val) {
                              setState(() {
                                permission.viewAllOrders = val;
                              });
                            }
                          : null,
                    ),
                    title: Text('View all orders'),
                    subtitle: Text(
                        'When off, the user can see only their assigned orders.'),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 30.0),
                    child: Text('Orders'),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0),
                    child: Row(
                      children: [
                        Checkbox(
                          value:!workflowOrders?false: permission.editAuthorizedOrders,
                          onChanged: workflowOrders
                              ? (val) {
                                  setState(() {
                                    permission.editAuthorizedOrders = val;
                                  });
                                }
                              : null,
                        ),
                        Text('Edit authorized orders')
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0),
                    child: Row(
                      children: [
                        Checkbox(
                          value:!workflowOrders?false: permission.editOrdersUponConvertingToInvoice,
                          onChanged: workflowOrders
                              ? (val) {
                                  setState(() {
                                    permission
                                            .editOrdersUponConvertingToInvoice =
                                        val;
                                  });
                                }
                              : null,
                        ),
                        Text('Edit  orders upon converting to invoice')
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0),
                    child: Row(
                      children: [
                        Checkbox(
                          value:!workflowOrders?false :permission.viewPofitabilityInfo,
                          onChanged: workflowOrders
                              ? (val) {
                                  setState(() {
                                    permission.viewPofitabilityInfo = val;
                                  });
                                }
                              : null,
                        ),
                        Text('View Profitablity info'),
                      ],
                    ),
                  ),
                ],
              )
            ],
          ),
          Divider(
            thickness: 1.0,
          ),
          ExpansionTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                    child: Row(
                  children: [
                    Icon(Icons.keyboard_arrow_down),
                    Icon(
                      Icons.calendar_today,
                    ),
                    Text('Calendar'),
                  ],
                )),
                Switch(
                    value: calendar,
                    onChanged: (v) {
                      setState(() {
                        calendar = v;
                      });
                    }),
              ],
            ),
            trailing: SizedBox(),
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ListTile(
                    leading: Checkbox(
                      value:!calendar?false: permission.viewAllAppointments,
                      onChanged: calendar
                          ? (val) {
                              setState(() {
                                permission.viewAllAppointments = val;
                              });
                            }
                          : null,
                    ),
                    title: Text('View all appointments'),
                    subtitle: Text(
                        'When off, the user can see only their assigned appointments.'),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0),
                    child: Row(
                      children: [
                        Checkbox(
                          value:!calendar?false: permission.includeUserOnCalendar,
                          onChanged: calendar
                              ? (val) {
                                  setState(() {
                                    permission.includeUserOnCalendar = val;
                                  });
                                }
                              : null,
                        ),
                        Text('Include user on calendar')
                      ],
                    ),
                  ),
                ],
              )
            ],
          ),
          Divider(
            thickness: 1.0,
          ),
          ExpansionTile(
            title: Padding(
              padding: const EdgeInsets.only(left: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      child: Row(
                    children: [
                      Icon(
                        Icons.home,
                      ),
                      Text('Inventory'),
                    ],
                  )),
                  Switch(
                    value: inventory,
                    onChanged: (v) {
                      setState(() {
                        inventory = v;
                      });
                    },
                  ),
                ],
              ),
            ),
            trailing: SizedBox(),
            children: [],
          ),
          Divider(
            thickness: 1.0,
          ),
          ExpansionTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                    child: Row(
                  children: [
                    Icon(Icons.keyboard_arrow_down),
                    Icon(
                      Icons.timer,
                    ),
                    Text('Time Clocks'),
                  ],
                )),
                Switch(
                    value: timeClocks,
                    onChanged: (v) {
                      setState(() {
                        timeClocks = v;
                      });
                    }),
              ],
            ),
            trailing: SizedBox(),
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ListTile(
                    leading: Checkbox(
                      value:!timeClocks?false: permission.viewAllTimeClocks,
                      onChanged: timeClocks
                          ? (val) {
                              setState(() {
                                permission.viewAllTimeClocks = val;
                              });
                            }
                          : null,
                    ),
                    title: Text('View all time clocks'),
                    subtitle: Text(
                        'When off, the user can see only their time clocks.'),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0),
                    child: Row(
                      children: [
                        Checkbox(
                          value:!timeClocks?false: permission.editTimeSheetEntries,
                          onChanged: timeClocks
                              ? (val) {
                                  setState(() {
                                    permission.editTimeSheetEntries = val;
                                  });
                                }
                              : null,
                        ),
                        Text('Edit timesheet entries')
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0),
                    child: Row(
                      children: [
                        Checkbox(
                          value:!timeClocks?false: permission.addTimeManually,
                          onChanged: timeClocks
                              ? (val) {
                                  setState(() {
                                    permission.addTimeManually = val;
                                  });
                                }
                              : null,
                        ),
                        Text('Add time manually')
                      ],
                    ),
                  ),
                ],
              )
            ],
          ),
          Divider(
            thickness: 1.0,
          ),
          ExpansionTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                    child: Row(
                  children: [
                    Icon(Icons.keyboard_arrow_down),
                    Icon(
                      Icons.list,
                    ),
                    Text('Lists'),
                  ],
                )),
                Switch(
                    value: lists,
                    onChanged: (v) {
                      setState(() {
                        lists = v;
                      });
                    }),
              ],
            ),
            trailing: SizedBox(),
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0),
                    child: Row(
                      children: [
                        Checkbox(
                          value:!lists?false: permission.viewAndEditInspectionTemplates,
                          onChanged: lists
                              ? (val) {
                                  setState(() {
                                    permission.viewAndEditInspectionTemplates =
                                        val;
                                  });
                                }
                              : null,
                        ),
                        Text('View and edit inspection templates')
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0),
                    child: Row(
                      children: [
                        Checkbox(
                          value:!lists?false: permission.viewAndEditCannedServices,
                          onChanged: lists
                              ? (val) {
                                  setState(() {
                                    permission.viewAndEditCannedServices = val;
                                  });
                                }
                              : null,
                        ),
                        Text('View and edit canned services')
                      ],
                    ),
                  ),
                ],
              )
            ],
          ),
          Divider(
            thickness: 1.0,
          ),
          ExpansionTile(
            title: Padding(
              padding: const EdgeInsets.only(left: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      child: Row(
                    children: [
                      Icon(
                        Icons.insert_chart,
                      ),
                      Text('Reports'),
                    ],
                  )),
                  Switch(
                      value: reports,
                      onChanged: (v) {
                        setState(() {
                          reports = v;
                        });
                      }),
                ],
              ),
            ),
            trailing: SizedBox(),
            children: [],
          ),
          Divider(
            thickness: 1.0,
          ),
          ExpansionTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                    child: Row(
                  children: [
                    Icon(Icons.keyboard_arrow_down),
                    Icon(
                      Icons.settings,
                    ),
                    Text('Settings'),
                  ],
                )),
                Switch(
                    value: settings,
                    onChanged: (v) {
                      setState(() {
                        settings = v;
                      });
                    }),
              ],
            ),
            trailing: SizedBox(),
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ListTile(
                    leading: Checkbox(
                        value:!settings? false: permission.viewAndEditUsers,
                        onChanged:settings? (val) {
                          setState(() {
                            permission.viewAndEditUsers = val;
                          });
                        }:null),
                    title: Text('View and edit users'),
                    subtitle: Text('This includes adding and removing users.'),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0),
                    child: Row(
                      children: [
                        Checkbox(
                          value:!settings? false: permission.viewAndEditPricingMatrices,
                          onChanged:settings? (val) {
                            setState(() {
                              permission.viewAndEditPricingMatrices = val;
                            });
                          }:null,
                        ),
                        Text('View and edit pricing matrices')
                      ],
                    ),
                  ),
                ],
              )
            ],
          ),
        ],
      ),
    );
  }
}
