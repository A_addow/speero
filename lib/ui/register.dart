//import 'dart:convert';

import 'package:flutter/material.dart';
import '../models/user.dart';

import 'package:http/http.dart'
    as http; //as http means create object to use later

class RegisterPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return RegisterPageState();
  }
}

class RegisterPageState extends State<RegisterPage> {
  String firstName;
  String lastName;
  String email;
  String password;
  User user;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Register Page'),
      ),
      body: ListView(
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Align(
                alignment: Alignment.topLeft,
                child: Image.asset('assets/shopmonkeylogo.PNG'),
              ),
              SizedBox(
                height: 32,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
                child: Text(
                  'Start Your Free Trial',
                  style: TextStyle(fontSize: 25.0),
                ),
              ),
              ButtonTheme(
                minWidth: 320,
                height: 40.0,
                child: RaisedButton(
                  color: Color.fromRGBO(0, 108, 250, 1.0),
                  onPressed: () {},
                  child: Text(
                    'Sign Up With Google',
                    style: TextStyle(fontSize: 20, color: Colors.white),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                child: TextField(
                  onChanged: (fn) {
                    firstName = fn;
                  },
                  decoration: InputDecoration(
                    labelText: 'First Name',
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey, width: 0.5),
                    ),
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                child: TextField(
                  onChanged: (ln) {
                    lastName = ln;
                  },
                  decoration: InputDecoration(
                    labelText: 'Last Name',
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey, width: 0.5),
                    ),
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                child: TextField(
                  onChanged: (em) {
                    email = em;
                  },
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    labelText: 'Email',
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey, width: 0.5),
                    ),
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                child: TextField(
                  onChanged: (pa) {
                    password = pa;
                  },
                  keyboardType: TextInputType.visiblePassword,
                  decoration: InputDecoration(
                    labelText: 'Password',
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey, width: 0.5),
                    ),
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                child: TextField(
                  keyboardType: TextInputType.visiblePassword,
                  decoration: InputDecoration(
                    labelText: 'Confirm Password',
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey, width: 0.5),
                    ),
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              ButtonTheme(
                minWidth: 320,
                height: 40.0,
                child: RaisedButton(
                  onPressed: () {
                    user = User(
                      firstName: firstName,
                      lastName: lastName,
                      email: email,
                     // password: password,
                    );
                    var map = {
                      'first_name': user.firstName,
                      'last_name': user.lastName,
                      'email': user.email,
                      //'password': user.password
                    };
                    // map['firstName'] = user.firstName;
                    // map['lastName'] = user.lastName;
                    // map['email'] = user.email;
                    // map['password'] = user.password;
                    print(user.firstName);
                    http
                        .post('http://192.168.0.113:2003/v1/register',
                            body: map)
                        .then((http.Response response) {
                      print(response.body);
                    });
                  },
                  child: Text(
                    'Start Now',
                    style: TextStyle(fontSize: 20, color: Colors.white),
                  ),
                  color: Color.fromRGBO(15, 168, 115, 1.0),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
