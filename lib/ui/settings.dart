import 'package:flutter/material.dart';
import 'package:shop_monkey/widgets/list_tile.dart';

class Settings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(74, 89, 106, 0.75),
        leading: Row(
          children: [
            IconButton(
              icon: Icon(
                Icons.arrow_back_ios,
                size: 20.0,
              ),
              onPressed: () => Navigator.pop(context),
            ),
          ],
        ),
        centerTitle: true,
        title: Text('Settings'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                alignment: Alignment.topLeft,
                child: Text(
                  'Settings',
                  style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w200),
                ),
              ),
              FlatButton(
                color: Colors.blue[50],
                child: ListTile(
                  leading: Icon(
                    Icons.settings,
                    color: Colors.blueAccent,
                  ),
                  title: Text(
                    'General Settings',
                    style: TextStyle(
                      color: Colors.blueAccent,
                    ),
                  ),
                ),
                onPressed: () {},
              ),
              BuildListTile('Pricing Matrix', Icons.transform),
              BuildListTile('Billing', Icons.local_atm),
              BuildListTile('Users', Icons.person),
              BuildListTile('Work Request Forms', Icons.assignment),
              BuildListTile('Payments', Icons.monetization_on),
              Divider(
                thickness: 1.0,
              ),
              Container(
                alignment: Alignment.topLeft,
                // padding: EdgeInsets.only(left: 20.0),
                child: Text(
                  'Integrations',
                  style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w200),
                ),
              ),
              BuildListTile('Carfax', Icons.copyright),
              BuildListTile('QuickBooks', Icons.all_inclusive),
              BuildListTile('Parts Procurement', Icons.category),
            ],
          ),
        ),
      ),
    );
  }
}
