import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:shop_monkey/shared/my_shared_prefrences.dart';
import '../shared/constants.dart';
import '../models/user.dart';

class UpdateUser extends StatefulWidget {
  final User user;
  UpdateUser(this.user);
  @override
  _UpdateUserState createState() => _UpdateUserState();
}

class _UpdateUserState extends State<UpdateUser> {
  final _focusLastName = FocusNode();
  final _focusEmail = FocusNode();
  final _focusPhone = FocusNode();
  final _form = GlobalKey<FormState>();
  String firstName;
  String lastName;
  String email;
  String phone;
  @override
  void dispose() {
    _focusLastName.dispose();
    _focusEmail.dispose();
    _focusPhone.dispose();
    super.dispose();
  }

  void _saveData() {
    _form.currentState.save();
  }

  Future<void> _updateUser() async {
    var token = MySharedPrefrences.getByKey(Constants.MYTOKEN);

 Map<String, String> requestHeaders = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    }; //bearer is important
    var map = {};
    if (!(widget.user.firstName == firstName)) map['first_name'] = firstName;
    if (!(widget.user.lastName == lastName)) map['last_name'] = lastName;
    if (!(widget.user.email == email)) map['email'] = email;
    if (!(widget.user.phone == phone)) map['phone'] = phone;
    print(map['phone']);
    var response = await patch(
      Constants.BASICURL + Constants.USERS +widget.user.id,
      body: map,
      headers: requestHeaders,
    );
    print(json.decode(response.body));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Update User'),
        actions: [
          IconButton(
              icon: Icon(Icons.save),
              onPressed: () {
                _saveData();
                _updateUser();
              }),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _form,
          child: ListView(
            children: [
              TextFormField(
                initialValue: widget.user.firstName,
                decoration: InputDecoration(labelText: 'First Name'),
                textInputAction: TextInputAction.next,
                onFieldSubmitted: (_) {
                  FocusScope.of(context).requestFocus(_focusLastName);
                },
                onSaved: (fnam) => firstName = fnam,
                onChanged: (fname) => firstName = fname,
              ),
              TextFormField(
                initialValue: widget.user.lastName,
                decoration: InputDecoration(labelText: 'Last Name'),
                textInputAction: TextInputAction.next,
                focusNode: _focusLastName,
                onFieldSubmitted: (_) {
                  FocusScope.of(context).requestFocus(_focusEmail);
                },
                onSaved: (lnam) => lastName = lnam,
                onChanged: (lname) => lastName = lname,
              ),
              TextFormField(
                initialValue: widget.user.email,
                decoration: InputDecoration(labelText: 'Email'),
                textInputAction: TextInputAction.next,
                keyboardType: TextInputType.emailAddress,
                focusNode: _focusEmail,
                onFieldSubmitted: (_) {
                  FocusScope.of(context).requestFocus(_focusPhone);
                },
                onSaved: (emai) => email = emai,
                onChanged: (emai) => email = emai,
              ),
              TextFormField(
                initialValue: widget.user.phone,
                decoration: InputDecoration(labelText: 'Phone'),
                textInputAction: TextInputAction.done,
                keyboardType: TextInputType.phone,
                focusNode: _focusPhone,
                onFieldSubmitted: (_) => _saveData,
                onSaved: (phon) => phone = phon,
                onChanged: (phon) => phone = phon,
              )
            ],
          ),
        ),
      ),
    );
  }
}
