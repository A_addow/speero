import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import '../shared/constants.dart';
import '../shared/my_shared_prefrences.dart';
import '../ui/update_user.dart';
import '../models/user.dart';

class UserDeatails extends StatelessWidget {
  final User user;
  UserDeatails(this.user);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('User Details'),
      ),
      body: Container(
        alignment: Alignment.center,
        margin: EdgeInsets.only(top: 10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              'Id:' + user.id,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 12.0,
                  backgroundColor: Colors.deepOrange,
                  color: Colors.white70),
            ),
            Text(
              'FullName:' + user.firstName + ' ' + user.lastName,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 12.0,
              ),
            ),
            Text(
              'Email:' + user.email,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 12.0,
              ),
            ),
            Text(
              'Phone:${user.phone}',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 12.0,
              ),
            ),
            Text(
              'Role:' + user.role,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 12.0,
              ),
            ),
            Text(
              'IsActive:' + user.isActive.toString(),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 12.0,
              ),
            ),
            Text(
              'Rate:' + user.rate.toString(),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 12.0,
              ),
            ),
            Row(mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                IconButton(
                  icon: Icon(
                    Icons.edit,
                    color: Colors.deepOrange,
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (ctx) => UpdateUser(user),
                      ),
                    );
                  },
                ),
                IconButton(
                  icon: Icon(
                    Icons.delete,
                    color: Colors.red,
                  ),
                  onPressed: () {
                    showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                            title: Text('Are you sure?'),
                            content: Text('the operation can\'t undo'),
                            actions: [
                              FlatButton(
                                child: Text('no'),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                               FlatButton(
                                child: Text('yes'),
                                onPressed: () {
                                  deleteUser(user.id,context);
                                },
                              ),
                            ],
                          );
                        });
                  },
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Future<void> deleteUser(String id,context) async {
    var token = MySharedPrefrences.getByKey(Constants.MYTOKEN);

    Map<String, String> requestHeaders = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
      '_method':'delete'
    }; //bearer is important
   final ids=[];
   ids.add(id);
   ids.add('2b52fb22-59a8-328c-bde7-e2b873951418');
   var map;
   int lastIndex;
   for(int i=0; i< ids.length;i++){
     map={'ids[$i]':ids[i]};
     lastIndex=i;
   }
   lastIndex++;
   map={'ids[$lastIndex]':'0fed83fe-2025-3d81-8869-54afc7fd7808'};
   

   map['_method']='delete';//a trick used to delete multiple users using post method.
   
   var finalUrl=Constants.BASICURL + Constants.USERS;
    var response = await post(
      finalUrl,
      headers: requestHeaders,
      body: map
    );
    Navigator.of(context).pop();
    print(json.decode(response.body));
  }
}
