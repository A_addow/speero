import 'dart:convert';

import 'package:http/http.dart'
    as http; //as http means create object to use later

import 'package:flutter/material.dart';
import 'package:shop_monkey/widgets/notification_dialog.dart';
import 'package:shop_monkey/widgets/quick_categories.dart';
import '../widgets/datatable.dart';
import '../shared/constants.dart';
import '../shared/my_shared_prefrences.dart';
import '../ui/user_details.dart';
import '../models/user.dart';

class UsersList extends StatefulWidget {
  @override
  _UsersListState createState() => _UsersListState();
}

class _UsersListState extends State<UsersList> {
  bool isSearching = false;

  List<User> users = [];
  Future<List<User>> fetchUsers() async {
    const url = Constants.BASICURL + Constants.USERS;
    final tokens = await MySharedPrefrences.getByKey(Constants.MYTOKEN);
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $tokens'
    }; //bearer is important
    var response = await http.get(url, headers: requestHeaders);
    final extractedData = json.decode(response.body) as Map<String, dynamic>;
    print(extractedData['data']['users'][0]['first_name']);
    final usersData = extractedData['data']['users'];
    for (var user in usersData) {
      users.add(
        User(
          id: user['id'],
          email: user['email'],
          firstName: user['first_name'],
          lastName: user['last_name'],
          isActive: user['is_active'],
          phone: user['phone'],
          rate: user['rate'],
          role: user['role'],
        ),
      );
    }
    return users;
  }

  void uDetails(String id) {
    User uDetail = users.firstWhere((user) {
      return user.id.contains(id);
    });
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => UserDeatails(uDetail),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    print('......now');
    return Scaffold(
      drawer: Drawer(
      ),
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(74, 89, 106, 0.75),
        actions: [
          Container(
            width: 300.0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                  icon: Icon(Icons.search),
                  onPressed: () {
                    setState(() {
                      isSearching = !isSearching;
                    });
                  },
                  alignment: Alignment.centerLeft,
                ),
                if (!isSearching) ...[
                  IconButton(
                    icon: Image.asset(
                      'assets/logo.png',
                      fit: BoxFit.cover,
                    ),
                    onPressed: () {},
                  ),
                  IconButton(
                    icon: Icon(Icons.add),
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) => QuickCategories(),
                      );
                    },
                  ),
                  IconButton(
                      icon: Icon(Icons.notifications),
                      onPressed: () {
                        showDialog(
                          context: context,
                          builder: (BuildContext context) =>
                              NotificationDialog(),
                        );
                      }),
                ],
                if (isSearching)
                  Expanded(
                      child: TextField(
                    decoration: InputDecoration(
                      hintText: 'search here',
                    ),
                  )),
              ],
            ),
          ),
        ],
      ),
      body: FutureBuilder(
        future: fetchUsers(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.data == null) {
            return Center(child: CircularProgressIndicator());
          } else {
            return SingleChildScrollView(
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text(
                        'Users',
                        style: TextStyle(fontSize: 18.0),
                      ),
                      RaisedButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0)),
                        onPressed: () => Navigator.of(context).pushNamed('/CreateUser'),
                        child: Row(children: [
                          Icon(Icons.person, color: Colors.white),
                          Text(
                            'Add New User',
                            style: TextStyle(color: Colors.white),
                          ),
                        ]),
                        padding: EdgeInsets.all(3.0),
                        color: Colors.blue,
                      ),
                    ],
                  ),
                  Container(child: MyDatatable(snapshot.data)),
                ],
              ),
            );
          }
        },
      ),
    );
  }
}
