import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:shop_monkey/models/user.dart';

class MyDatatable extends StatelessWidget {
  final List<User> users;
  MyDatatable(this.users);
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: DataTable(
        columns: [
          DataColumn(
              label: Text('Active'),
              numeric: false,
              tooltip: 'this indicates weather the user is avtive or not.'),
          DataColumn(
              label: Text('First Name'),
              numeric: false,
              tooltip: 'this is the first name.'),
          DataColumn(
              label: Text('Last Name'),
              numeric: false,
              tooltip: 'this is the last name.'),
          DataColumn(
            label: Text('Phone'),
            numeric: true,
            tooltip: 'this is the phone.',
          ),
        ],
        rows: users
            .map((user)=>
              DataRow(
                cells: [
                  DataCell(Checkbox(value: user.isActive??false, onChanged: null)),
                  DataCell(
                    Text(user.firstName??''),
                  ),
                  DataCell(
                    Text(user.lastName??''),
                  ),
                  DataCell(
                    Text(user.phone==null?'' :user.phone),
                  ),
                ],
              ),
            )
            .toList(),
      ),
    );
  }
}
