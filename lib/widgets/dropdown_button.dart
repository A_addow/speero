import 'package:flutter/material.dart';

class MyDropDownButton extends StatefulWidget {
  final List<String> _roles;
  final String title;
  final Function notifySelectedValue;
  MyDropDownButton(this._roles, this.title,this.notifySelectedValue);

  @override
  _MyDropDownButtonState createState() => _MyDropDownButtonState();
}

class _MyDropDownButtonState extends State<MyDropDownButton> {
  String selectedValue;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Text(widget.title),
      title: DropdownButton(
        items: widget._roles
            .map(
              (value) => DropdownMenuItem(
                child: Text(value),
                value: value,
              ),
            )
            .toList(),
        onChanged: (selectedType) {
          setState(() {
            selectedValue = selectedType;
            widget.notifySelectedValue(selectedValue);
          });
        },
        value: selectedValue,
        hint:widget._roles.contains('Permissions')?Text('Permissions'): Text('select type'),
      ),
    );
  }
}
