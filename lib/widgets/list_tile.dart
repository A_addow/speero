import 'package:flutter/material.dart';

class BuildListTile extends StatelessWidget {
  final String title;
  final IconData icon;
  BuildListTile(this.title, this.icon);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: CircleAvatar(
        child: Icon(icon),
      ),
      title: Text(title),
      onTap:title=='Users'? (){Navigator.of(context).pushReplacementNamed('/$title');}:title=='Settings'?(){Navigator.of(context).pushNamed('/$title');}: ()=>SizedBox() //(){Navigator.of(context).pushNamed('/$title');}
    );
  }
}
