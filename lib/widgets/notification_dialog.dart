import 'package:flutter/material.dart';

class NotificationDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Container(
        height: 240.0,
        width: 240.0,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(padding: EdgeInsets.all(2),
              alignment: Alignment.centerLeft,
              child: Text(
                "Notifications",
              ),
            ),
            Divider(
              thickness: 1.5,
            ),
            Container(
                margin: EdgeInsets.all(15),
                alignment: Alignment.center,
                child: Text('Hang Tight')),
            Icon(Icons.notifications_active),
            Container(
              padding: EdgeInsets.only(left:16.0),
              child: Text(
                'Notifications will show up when customers approve jobs or add notes.',
              ),
            ),
            
          ],
        ),
      ),
    );
  }
}
