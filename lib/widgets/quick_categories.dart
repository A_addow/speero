import 'package:flutter/material.dart';

class QuickCategories extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Container(
          height: 210.0,
          width: 370.0,
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.all(3.0),
                child: Row(
                  children: <Widget>[
                    RaisedButton(
                      color: Colors.grey[100],
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        height: 70.0,
                        width: 69.0,
                        child: Column(
                          children: [
                            Icon(Icons.person),
                            Text('Customers & Vehicle'),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 2, left: 2),
                      child: RaisedButton(
                        color: Colors.grey[100],
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Container(
                          height: 70.0,
                          width: 69.0,
                          child: Column(
                            children: [
                              Icon(Icons.event_note),
                              Text('Estimate'),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 2, left: 2),
                      child: RaisedButton(
                        color: Colors.grey[100],
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Container(
                          height: 70.0,
                          width: 69.0,
                          child: Column(
                            children: [
                              Icon(Icons.calendar_today),
                              Text('Appointment'),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 2),
                    child: RaisedButton(
                      color: Colors.grey[100],
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        height: 70.0,
                        width: 69.0,
                        child: Column(
                          children: [
                            Icon(Icons.dashboard),
                            Text('Inventory part'),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 2),
                    child: RaisedButton(
                      color: Colors.grey[100],
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        height: 70.0,
                        width: 69.0,
                        child: Column(
                          children: [
                            Icon(Icons.directions_car),
                            Text('Fleet'),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 2),
                    child: RaisedButton(
                      color: Colors.grey[100],
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        height: 70.0,
                        width: 69.0,
                        child: Column(
                          children: [
                            Icon(Icons.event_note),
                            Text('Estimate'),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              ListTile(
                  leading: Icon(Icons.image), title: Text('Services & Diagrams Lookup'))
            ],
          ),
        ),
      ),
    );
  }
}
