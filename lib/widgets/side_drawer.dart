import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shop_monkey/widgets/list_tile.dart';

class SideDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.red, //or set color with: Color(0xFF0000FF)
    ));
    return Drawer(
      child: Container(
        color: Color.fromRGBO(74, 89, 106, 0.75),
        margin: EdgeInsets.only(top: 20.0),
        child: SingleChildScrollView(
                  child: Column(
            children: [
              Row(
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.close,
                      color: Colors.white,
                    ),
                    onPressed: () {Navigator.pop(context);},
                  ),
                  IconButton(
                    icon: Image.asset(
                      'assets/logo.png',
                      fit: BoxFit.cover,
                    ),
                    onPressed: () {},
                  ),
                ],
              ),
              SizedBox(
                height: 15.0,
              ),
              ListTile(
                leading: CircleAvatar(
                  child: Text('dp'),
                ),
                title: Text('dis possable77'),
                subtitle: Text('dfxgfds'),
                trailing: IconButton(
                  icon: Icon(
                    Icons.keyboard_arrow_down,
                    color: Colors.white,
                  ),
                  onPressed: () {},
                ),
                onTap: () {},
              ),
              Divider(
                color: Colors.white,
                thickness: 1.0,
              ),
                ListTile(
                leading: CircleAvatar(
                  child: Text('15%'),
                ),
                title: Text('1 of 7 steps completed'),
                subtitle: Text('Account Setup'),
                trailing: IconButton(
                  icon: Icon(
                    Icons.keyboard_arrow_down,
                    color: Colors.white,
                  ),
                  onPressed: () {},
                ),
                onTap: () {},
              ),
              Divider(
                color: Colors.white,
                thickness: 1.0,
              ),
                            FlatButton.icon(onPressed: null, icon: Icon(Icons.explore, ),label: Text('Welcome'),color: Colors.pink,),

              BuildListTile('Dashboard', Icons.av_timer),
              BuildListTile('Workflow', Icons.developer_board),
              BuildListTile('Calendar', Icons.calendar_today),
              BuildListTile('Inventory', Icons.home),
              BuildListTile('Time Clocks', Icons.access_time),
              BuildListTile('Lists', Icons.list),
              BuildListTile('Reports', Icons.poll),
              BuildListTile('Settings', Icons.settings),
            ],
          ),
        ),
      ),
    );
  }
}
