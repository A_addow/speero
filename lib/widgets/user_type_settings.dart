import 'package:flutter/material.dart';

class UserTypeSettings extends StatelessWidget {
  final String title,subTitle;
  final bool value;
  UserTypeSettings(this.title,this.subTitle,this.value);
  @override
  Widget build(BuildContext context) {
    return Column(children: [
      ListTile(
        title: Text(title),
        subtitle: Text(subTitle),
        trailing: Switch(value: value, onChanged: (val){}),
      ),
    ],);
  }
}